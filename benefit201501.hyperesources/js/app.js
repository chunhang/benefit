
function Game(){
  this.startBtn = util.$id('game-start')
  this.container = util.$id('game-container')
  this.playBtn = util.$id('game-play')
  this.items = util.$id('game-items')
  this.li = util.$$('li', this.items)
  this.gameScore = util.$id('game-score')
  this.gameTimer = util.$id('game-timer')
  this.gameResult = util.$id('game-result')
  this.resultScore = util.$id('result-score')
  this.resultGift = util.$id('result-gift')
  this.gameLink = util.$id('game-link')
  this.gameReplay = util.$id('game-replay')
  this.gameForm = util.$id('game-form')
  this.started = false
  this.posArr = ['-10%', '0', '10%', '20%', '30', '40%', '50%', '60%', '70%', '80%']
  this.li = this.posArr.slice.call(this.li)
  this.init()
}

Game.prototype.init = function(){
  var that = this

  var animationend = this.whichAnimationEvent()
  this.li.forEach(function(li){
    util.on(li, animationend, function(e){
      that.restartAnimation(li)
    })
  })

  this.disableScroll().preload()
  util.on(this.startBtn, 'click', function(){
    that.playBtn.classList.add('out')
    that.start()
  }).on(this.items, 'touchstart', function(){
    onfire.apply(that, arguments)
  }).on(document, 'touchstart', function(e){
    var t2 = e.timeStamp
    var t1 = document.lashtouch || t2
    var dt = t2 - t1
    var len = e.touches.length
    document.lashtouch = t2
    if(!dt || dt > 500 || len > 1) return
    e.preventDefault()

    if(!that.started) return
    that.container.classList.add('fire')
    setTimeout(function(){
      that.container.classList.remove('fire')
    }, 100)
  }).on(this.gameReplay, 'click', function(){
    that.gameResult.classList.add('out')
    that.start()
  }).on(this.gameLink, 'click', function(){
    that.gameResult.classList.add('out')
    that.gameForm.classList.remove('out')
  })

  function onfire(e){
    var target = e.target
    var that = this
    var name = target.tagName.toLowerCase()
    if(name === 'li'){
      target.classList.add('shot')
      this.score += 10
      this.gameScore.innerText = this.handleScore(this.score)
      setTimeout(function(){
        target.classList.remove('shot')
        that.restartAnimation(target)
      }, 100)
    }
  }

  return this
}

Game.prototype.restartAnimation = function(li){
  var that = this
  li.classList.remove('zoomin-level' + that.level)
  setTimeout(function(){
    that.setRandomPosition(li)
    li.classList.add('zoomin-level' + that.level)
  }, 30)
}

Game.prototype.whichAnimationEvent = function(){
  var t
  var el = document.createElement('fakeelement')
  var transitions = {
    "WebkitAnimation" : "webkitAnimationEnd",
    "MozAnimation"    : "animationend",
    "OAnimation"      : "oAnimationEnd",
    "msAnimation"     : "MSAnimationEnd",
    "animation"       : "animationend"
  }
  for(t in transitions){
    if( el.style[t] !== undefined ){
      return transitions[t]
    }
  }
}

Game.prototype.setRandomPosition = function(li){
  var index = this.getRandom(0, this.posArr.length - 1)
  li.style.right = this.posArr[index]
}

Game.prototype.getRandom = function(min, max){
  return Math.floor(Math.random() * (max - min + 1)) + min
}

Game.prototype.disableScroll = function(){
  var startX, startY

  document.addEventListener("touchstart", touchstart)  
  function touchstart(event) {
    var touches = event.touches
    if (touches && touches.length) {
      startX = touches[0].pageX
      startY = touches[0].pageY
      document.addEventListener("touchmove", touchmove)
    }
  }

  function touchmove(event) {
    var touches = event.touches
    if (touches && touches.length) {
      event.preventDefault()
      var deltaX = startX - touches[0].pageX
      var deltaY = startY - touches[0].pageY

      if (Math.abs(deltaX) >= 50 || Math.abs(deltaY) >= 50) {
        document.removeEventListener('touchmove', touchmove)
      }
    }
  }
  return this
}

Game.prototype.preload = function(){
  var list = [
    'img/game-result.png',
    'img/how-to-play.png',
    'img/link.png',
    'img/play-btn.png',
    'img/replay.png',
    'img/self.png',
    'img/send.png',
    'img/sina.png',
    'img/wechat.png',
    'g-peng.png',
    'g-qiang-0.png',
    'fire.png',
    'g-p1.png',
    'g-p2.png',
    'g-p3.png',
    'g-p4.png',
  ]
  for(var i = list.length; i --;){
    var img = new Image
    img.src = list[i]
  }
  return this
}

Game.prototype.handleLevel = function(rest){
  if(18 < rest && rest <= 25 && this.level !== 2){
    this.level = 2
    this.levelUp(2)
  }
  if(10 < rest && rest <= 18 && this.level !== 3){
    this.level = 3
    this.levelUp(3)
  }
  if(rest <= 10 && this.level !== 4){
    this.level = 4
    this.levelUp(4)
  }
}

Game.prototype.levelUp = function(level){
  this.li.forEach(function(li){
    var cls = li.getAttribute('rel')
    li.className = cls + ' zoomin-level' + level
  })
}

Game.prototype.handleScore = function(score){
  if(score > 9999) return score
  score = '' + score
  var rest = 4 - score.length
  var zero = []
  for(var i = 0; i < rest; i ++){
    zero[zero.length] = '0'
  }
  zero.push(score)
  return zero.join('')
}

Game.prototype.run = function(){
  this.rest = 30
  this.score = 0
  this.level = 1
  this.gameTimer.innerText = this.rest

  this.li.forEach(function(li){
    li.classList.add('zoomin-level1')
  })
  this.started = true
  var that = this
  this.timer = setInterval(function(){
    if(that.rest <= 0){
      clearInterval(that.timer)
      return that.gameOver()
    }
    that.gameTimer.innerText = -- that.rest
    that.handleLevel(that.rest)
  }, 1000)
}

Game.prototype.gameOver = function(){
  var that = this
  this.li.forEach(function(li){
    li.className = ''
    li.className = li.getAttribute('rel')
  })
  that.started = false
  this.resultScore.innerText = this.score
  this.setGift(this.score)
  util.fadeOut(this.container, {
    duration: 10,
    complete: function(){
      that.gameScore.innerText = '0000'
      that.gameResult.classList.remove('out')
    }
  })
}

Game.prototype.setGift = function(score){
  var gift
  if(score < 200){
    gift = 1
  } else if(200 <= score && score < 400){
    gift = 2
  } else if(400 <= score && score < 600){
    gift = 3
  } else if(600 <= score && score < 800){
    gift = 4
  } else if(800 <= score && score < 1000){
    gift = 5
  } else if(1000 <= score){
    gift = 6
  }
  this.resultGift.innerText = gift
}

Game.prototype.start = function(){
  util.fadeIn(this.container, {
    duration: 500,
    complete: this.run.bind(this)
  })
}

util.ready(function(){
  window.game = new Game
})

util.on(window, 'load', function(){
  util.remove(util.$id('loading'))
  util.show(util.$('div.game-wrapper'))
})
