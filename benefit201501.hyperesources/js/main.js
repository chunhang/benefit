"use strict";
var util;

util = {
  $id: function(selector) {
    return document.getElementById(selector);
  },
  $: function(selector, context) {
    return (context || document).querySelector(selector);
  },
  $$: function(selector, context) {
    return (context || document).querySelectorAll(selector);
  },
  show: function(ele) {
    if (ele != null) {
      ele.style.display = 'block';
    }
    return this;
  },
  hide: function(ele) {
    if (ele != null) {
      ele.style.display = 'none';
    }
    return this;
  },
  addClass: function(ele, cls) {
    if (ele != null) {
      ele.classList.add(cls);
    }
    return this;
  },
  removeClass: function(ele, cls) {
    if (ele != null) {
      ele.classList.remove(cls);
    }
    return this;
  },
  toggleClass: function(ele, cls) {
    if (ele != null) {
      ele.classList.toggle(cls);
    }
    return this;
  },
  on: function(ele, type, hanlder) {
    if (ele != null) {
      ele.addEventListener(type, hanlder, false);
    }
    return this;
  },
  off: function(ele, type, hanlder) {
    if (ele != null) {
      ele.removeEventListener(type, hanlder, false);
    }
    return this;
  },
  ready: function(hanlder) {
    return this.on(window, 'DOMContentLoaded', hanlder);
  },
  ajax: function(type, url, data, success) {
    var key, text, xhr;
    if (typeof data === 'function') {
      success = data;
      data = {};
    }
    xhr = new XMLHttpRequest;
    xhr.open(type, url, false);
    xhr.onreadystatechange = function() {
      if (xhr.status === 200) {
        if (data.type === 'json') {
          return success(JSON.parse(xhr.responseText));
        } else if (data.type === 'xml') {
          return success(xhr.responseXML);
        } else {
          return success(xhr.responseText);
        }
      }
    };
    if (type === 'POST') {
      text = '';
      for (key in data) {
        text += key + '=' + encodeURIComponent(data[key]);
      }
      xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      return xhr.send(text);
    } else {
      return xhr.send(data);
    }
  },
  load: function(url, data, success) {
    return this.ajax('GET', url, data, success);
  },
  post: function(url, data, success) {
    return this.ajax('POST', url, data, success);
  },
  remove: function(ele) {
    ele && ele.parentNode && ele.parentNode.removeChild(ele);
    return this;
  },
  easing: {
    linear: function(progress) {
      return progress;
    },
    quadratic: function(progress) {
      return Math.pow(progress, 2);
    },
    swing: function(progress) {
      return 0.5 - Math.cos(progress * Math.PI) / 2;
    },
    circ: function(progress) {
      return 1 - Math.sin(Math.acos(progress));
    },
    back: function(progress, x) {
      return Math.pow(progress, 2) * ((x + 1) * progress - x);
    },
    bounce: function(progress) {
      for (var a = 0, b = 1, result; 1; a += b, b /= 2) {
        if (progress >= (7 - 4 * a) / 11) {
          return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
        }
      }
    },
    elastic: function(progress, x) {
      return Math.pow(2, 10 * (progress - 1)) * Math.cos(20 * Math.PI * x / 3 * progress);
    }
  },
  animate: function(options) {
    var start = new Date;
    var id = setInterval(function() {
      var timePassed = new Date - start;
      var progress = timePassed / options.duration;
      if (progress > 1) {
        progress = 1;
      }
      options.progress = progress;
      var delta = options.delta(progress);
      options.step(delta);
      if (progress == 1) {
        clearInterval(id);
        options.complete();
      }
    }, options.delay || 10);
  },
  fadeOut: function(element, options) {
    var to = 1;
    var that = this;
    if (!element) return this;
    this.animate({
      duration: options.duration,
      delta: function(progress) {
        progress = this.progress;
        return that.easing.swing(progress);
      },
      complete: options.complete,
      step: function(delta) {
        element.style.opacity = to - delta;
      }
    });
  },
  fadeIn: function(element, options) {
    var to = 0;
    var that = this;
    if (!element) return this;
    this.animate({
      duration: options.duration,
      delta: function(progress) {
        progress = this.progress;
        return that.easing.swing(progress);
      },
      complete: options.complete,
      step: function(delta) {
        element.style.opacity = to + delta;
      }
    });
  }
};
